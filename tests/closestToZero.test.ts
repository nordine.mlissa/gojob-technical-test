import closestToZero from '../src/closestToZero'

describe('closesToZero', () => {
    test('All positive numbers', () => {
        expect(closestToZero([8, 5, 10])).toBe(5)
    })
    test('Positive and negative numbers', () => {
        expect(closestToZero([5, 4, -9, 6, -10, -1, 8])).toBe(-1)
    })
    test('Prefer positive', () => {
        expect(closestToZero([8, 2, 3, -2])).toBe(2)
    })
    test('Returns 0 when input array is undefined or empty', () => {
        // @ts-ignore because the input argument is purposely not provided
        expect(closestToZero()).toBe(0)
        expect(closestToZero([])).toBe(0)
    })
})


# closestToZero 

Finds the closest number to zero

# Installation
```
$ yarn add https://gitlab.com/nordine.mlissa/gojob-technical-test
```

# Usage

```
const closestToZero = require('closest-to-zero')

console.log(closestToZero([4, -6, 3, -1])) // -1
```

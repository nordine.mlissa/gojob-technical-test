module.exports = {
    transform: {
      '.ts': 'ts-jest',
    },
    testPathIgnorePatterns: ['/node_modules/'],
    testRegex: '.test.ts$',
    moduleFileExtensions: ['js', 'ts'],
  }
  
type ClosestToZero = (input: number[]) => number;

/**
 * Returns the closest number to zero
 * @param input Array of numbers
 */
const closestToZero: ClosestToZero = (input = []) => {
  let closest: number = 0;

  // Early exit if the input isn't suited
  if (!input || !Array.isArray(input)) {
    return closest;
  }

  input.forEach(number => {
    const numberPositive = Math.abs(number);
    const closestPositive = Math.abs(closest);

    if (
      !closest || // no zero
      numberPositive < closestPositive // lower than current number
    ) {
      closest = numberPositive === closest
        ? numberPositive // prefer positive
        : number; 
    }
  });

  return closest;
};

export = closestToZero;

declare type ClosestToZero = (input: number[]) => number;
/**
 * Returns the closest number to zero
 * @param input Array of numbers
 */
declare const closestToZero: ClosestToZero;
export = closestToZero;

"use strict";
/**
 * Returns the closest number to zero
 * @param input Array of numbers
 */
var closestToZero = function (input) {
    if (input === void 0) { input = []; }
    var closest = 0;
    // Early exit if the input isn't suited
    if (!input || !Array.isArray(input)) {
        return closest;
    }
    input.forEach(function (number) {
        var numberPositive = Math.abs(number);
        var closestPositive = Math.abs(closest);
        if (!closest || // no zero
            numberPositive < closestPositive // lower than current number
        ) {
            closest = numberPositive === closest
                ? numberPositive // prefer positive
                : number;
        }
    });
    return closest;
};
module.exports = closestToZero;
